﻿//============================================================================
// Name        : lab11.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include <stdio.h>
#include <math.h>

#define pi 3.141

using namespace std;

int main()
{

printf("This is a test.\n\n");			

			// Problem 1
    float radius=-1;
    float area=-1;
    printf("\nPlease enter the Radius of the circle:");
    scanf("%f",&radius);
    while( radius < 0)
    {
        printf("\nInvalid entry.Please re-enter the Radius:");
        scanf("%f",&radius);
    }
    area=pi*radius*radius;
    printf("\nThe area is %.2f.\n\n",area);
	
								//End of problem 1
								
								// Problem 2

    float cost;
    float sell;
    printf("\nPlease enter the cost price of the item:");
    scanf("%f",&cost);

    while( cost < 0)
    {
        printf("\nInvalid entry.Please re-enter the Cost again:");
        scanf("%f",&cost);
    }

    printf("\nPlease enter the selling price of the item:");
    scanf("%f",&sell);

    while( sell < 0)
    {
        printf("\nInvalid entry.Please re-enter the Selling price again:");
        scanf("%f",&sell);
    }

    if((sell-cost) < 0)
        printf("\nI'm sorry you lost %.2f",(sell-cost));
    else if((sell-cost) == 0)
        printf("\nYou had no gains or losses.\n");
    else
        printf("You profited %.2f from your sale.\n",(sell-cost));


								//End of problem 2
								// Problem 2

    char op;
    float x;
    float y;
    float result;

    printf("\nPlease enter two numbers and an operator:");
    scanf("%f %c %f",&x,&op,&y);

    if(op != '+' && op != '-'&& op != '/' && op != '*')
    {
        printf("\nInvalid operator...cannot compute\n");
        return 0;
    }


    switch(op)
    {

        case '+':

            result = x+y;

            break;

        case '-':

            result=x-y;

            break;

        case '/':

            if(y == 0)
            {
                printf("\n Result is undef \n");
                return 0;
            }
            result = x/y;

            break;

        case '*':

            result = x*y;

            break;
    }

    printf("\nResult is %.2f\n",result);

								//End of problem 2
    return 0;
}
								//End of program